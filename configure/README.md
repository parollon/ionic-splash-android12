# Capacitor configure
Capacitor configuration tool . You can configure your project without modifying excplicitly the iOS or Android files. Visit [capacitor-configure repository](https://github.com/ionic-team/capacitor-configure) for details.
```bash
#installation
$ npm install @capacitor/project
# remember to set env variable JAVA_HOME
```
```bash
$ npx tsc configure/configure.ts
$ node configure/configure.js
```
Little modification in `tsconfig.json`
```json
{
	...
	"compilerOptions": {
		...
		"target": "esnext",
		"module": "esnext",
		"lib": [..., "dom"]
		...
	},
	...
	
}
```
#### Documentation
Capacitor configure repository:
- Github: https://github.com/ionic-team/capacitor-configure
