import { CapacitorProject } from '@capacitor/project';
import { CapacitorConfig } from '@capacitor/cli';

// This takes a CapacitorConfig, such as the one in capacitor.config.ts, but only needs a few properties
// to know where the ios and android projects are
const config: CapacitorConfig = {
  ios: {
    path: 'ios',
  },
  android: {
    path: 'android',
  },
};

const configureProject= async ()=>{
  const display_name="Splash Android 12"
  const versionName="0.0.2"
  const packageName="com.test.splash" //Temporal??
  const androidVersionCode = 2

  const project = new CapacitorProject(config);
  await project.load();
  console.log(project);
  /* Android */
  await project.android?.setPackageName(packageName); 
  await project.android?.setVersionName(versionName);
  await project.android?.setVersionCode(androidVersionCode);
  
  /* Permissos al Manifest d'exemple
  project.android?.getAndroidManifest().injectFragment(
    'manifest',
    `
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-feature android:name="android.hardware.location.gps" />
    `,
  );*/

  /* iOS */
  //const appTarget = project.ios?.getAppTarget();
  //project.ios.setBundleId(appTarget.name,null , packageName) 
  //await project.ios.setVersion(appTarget.name,null,versionName);
  //ho gestiona el Xcode
  //await project.ios?.setBuild( appTarget.name, null, 11); 
  
  /*  Permissos al Info.plist d'exemple
  await project.ios?.updateInfoPlist(appTarget.name,null,{
    NSLocationWhenInUseUsageDescription:" Es necessita accedir a la seva posició per obtenir la posició",
    NSLocationAlwaysUsageDescription:"Es necessita accedir a la seva posició per obtenir la posició",

    NSCameraUsageDescription:"Es necessita accedir a la càmera",
    NSPhotoLibraryUsageDescription:"Es necessita accedir a la llibreria multimedia ",
    NSPhotoLibraryAddUsageDescription:"Es necessita accedir a la llibreria multimedia",
  });*/
  project.vfs.commitAll()


}

configureProject()
